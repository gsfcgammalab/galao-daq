# VATA4XX Helper Functions
# Because why the hell didn't they do this to beghin with?
# Written by: Sean Griffin

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')

import time
import datetime

import tbScript
import tb_product as vata4xx
import tbScript
import tbMath
from __main__ import tb as tb


def test_help():
    print("I am alive: " + str(tb))
    disable_readout()
    print("Readout disabled!")

def disable_readout():
    tb.writeSysReg(vata4xx.addr_cfg_readout_en, 0) 
    print("Readout disabled.") 

def enable_readout(reset=False):
    if reset is True:      
        print("Resetting readout counts.")
        tb.resetReadoutData()
        tbScript.confDelay()
        
    tb.writeSysReg(vata4xx.addr_cfg_readout_en, 1)  
    print("Readout enabled.")

def set_interface_mode(interface_mode):
    tb.writeSysReg(vata4xx.addr_cfg_InterfaceMode, interface_mode)

def setup_histogram(n_bins, x_min, x_max):
    tb.setHistogramMaxValue(x_max)
    tb.setHistogramNumBins(n_bins)
    tb.setHistogramMinValue(x_min)

def start_new_log(datalog_new_filename = None, enable_datalogging = True):
    print("Closing old log file and starting a new one.")
    if datalog_new_filename is None:
        datalog_new_filename = datetime.datetime.now().strftime('.\Logs\%Y-%m-%d__%H_%M_%S__VATA4xx__raw_data_log.csv')

    print("New datalog filename: ", datalog_new_filename)
    tb.enableDataLogging(False)
    tb.newDataLogFile(datalog_new_filename)
    if enable_datalogging:
        print("Logging enabled.")
        tb.enableDataLogging(True)

def conf_external_cal_pulse(channel, amplitude):
    '''
    Configure the external (Galao) calibration pulse).
    '''
    vata4xx.confCalDAC(amplitude)
    vata4xx.confCalPulse(0, channel, False, False)
    


def set_cal_pulse_enabled(channel, enable):

    print("VATA460 HELP: Toggling Galao calibration pulse to {0:s}".format("Enabled" if enable else "Disable"))

    vata4xx.confCalPulse(0, channel, enable, 0)
    vata4xx.confSysCalPulse(enable)

def set_cal_DAC_value(amplitude):

    print("VATA460 HELP: Configure Cal DAC = {0:d}".format(amplitude))
    tb.writeSysReg(vata4xx.addr_cfg_CalDac, amplitude)        


def make_channel_mask(disable_list):
    '''
    Creates a channel mask. 

    disable_list: list of channels to be disabled.

    '''
    channel_disable_mask = ['0' for i in range(32)]
    
    for ch in disable_list:
        channel_disable_mask[ch] = "1"

    channel_disable_mask = "".join(channel_disable_mask)

    return channel_disable_mask

def configure_asic():
    vata4xx.confWriteConfig(0)
    vata4xx.confWriteConfig(0)
    time.sleep(0.1)


def set_start_config():


    # Turn on Data Processing / GUI functions (for baseline readout)
    tb.enableGuiUpdates(True)
    tb.enablePlotUpdates(True)
    tb.enableDataProcessing(True)
    tb.enableHistograms(True)

    #Setup Histogram
    setup_histogram(1024, 0, 1023)

    #Disable Readout
    tb.enableDataLogging(False)
    disable_readout()    

    # ---------- SETTTINGS -------------
    #These are all default values; some are modified later. 

    vata4xx.CAL_PULSE_SOURCE = 0 # 0 = Galao, 1 = ASIC

    #ASIC
    vata4xx.WRITE_DEFALT_ASIC_CONFIG = 1
    vata4xx.ENABLE_ALL_CH_READOUT = False
    vata4xx.ASIC_NEGATIVE_TRIGGER_POLARITY = False 
    vata4xx.ASIC_DIGITAL_THRESHOLD = 0 
    vata4xx.ASIC_TRIGGER_THRESHOLD = 10
    vata4xx.CAL_PULSE_TRIG_DELAY   = 30
    vata4xx.INTERNAL_DAC_SETTING    = 50

    #Galao
    vata4xx.CAL_PULSE_TEST_CHANNEL = 16
    vata4xx.SYS_DAC_SETTING        = 1200
    vata4xx.CAL_PULSE_INTERVAL     = 10.e4 *2. -1 # 500 Hz
    vata4xx.CAL_PULSE_LENGTH       = ((vata4xx.CAL_PULSE_INTERVAL + 1)/2)      -1 #50% duty cycle    


def load_and_set_config(configuration):

    print("Setting ASIC configuration...")
    ### Configure ASIC
    cfg = configuration['ASIC_settings'] 
    print(cfg.keys())
    vata4xx.WRITE_DEFALT_ASIC_CONFIG = 1 #Force write default ASIC configuration. 
    vata4xx.ENABLE_ALL_CH_READOUT = cfg['enable_all_ch_readout']
    vata4xx.ASIC_NEGATIVE_TRIGGER_POLARITY = True if cfg['polarity']  < 0  else False
    vata4xx.ASIC_DIGITAL_THRESHOLD = cfg['digital_threshold']
    vata4xx.ASIC_TRIGGER_THRESHOLD = cfg['trigger_threshold']
    vata4xx.CAL_PULSE_TEST_CHANNEL = 15 #may get overwritten depending on run type

    tb.setAsicConfigBitFieldByAddr(17, cfg['disable_CM_subtraction'], 0)

    ### Configure ASIC dynamic range
    cfg = cfg['iramp_settings']
    vata4xx.ASIC_IRAMP_DAC = cfg['DAC']
    vata4xx.ASIC_IRAMP_FB  = cfg['FB']
    vata4xx.ASIC_IRAMP_F2  = cfg['F2']
    vata4xx.ASIC_IRAMP_F3  = cfg['F3']


    print("-----------------------------------")
    print("Setting QI configuration...")
    ### Configure QI; applies to ASIC if internal calibrator is being used.
    cfg = configuration['QI_settings']
    vata4xx.CAL_PULSE_SOURCE = cfg['cal_source'] # 0 = external, 1 = internal

    if vata4xx.CAL_PULSE_SOURCE == 0:
        print("*** Using external calibrator.")
        vata4xx.SYS_DAC_SETTING        = cfg['SYS_DAC_SETTING']
    else:
        print("*** Using internal calibrator.")
        vata4xx.INTERNAL_DAC_SETTING   = cfg['INTERNAL_DAC_SETTING']

    vata4xx.CAL_PULSE_TEST_CHANNEL = cfg['target_channel']
    vata4xx.CAL_PULSE_INTERVAL     = cfg['pulse_interval'] -1 
    vata4xx.CAL_PULSE_LENGTH       = cfg['pulse_length'] -1
    #vata4xx.CAL_PULSE_TRIG_DELAY   = cfg['trigger_delay']
    vata4xx.ENABLE_CAL_PULSE       = cfg['enable_cal_pulse'] 
    tb.writeSysReg(vata4xx.addr_cfg_forced_en, cfg['forced_trigger_readout'])


    vata4xx.ConfigureAsic(0) #writes registers
    configure_asic() #commits send
    vata4xx.ConfigureSystem()
    configure_asic() #commits send


def write_cfg_file_copy(dataset_name, basename, target_cfg):

    if dataset_name is not None:
        print("Copying configuration file to {0:s}".format(target_cfg))
        if os.path.isfile(target_cfg):
            raise Exception("Config file exists! Aborting run.")
        if not os.path.exists(os.path.dirname(target_cfg)):
            try:
                os.makedirs(os.path.dirname(target_cfg))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise        
        copyfile(config_filename, target_cfg)
    else:
        print("----> Not writing configuration file.")

def clear_existing_target_file(write_file):
    if write_file is not None and os.path.isfile(write_file):
        vh.start_new_log()
        print("Deleting existing file...")
        os.remove(write_file) 
        
