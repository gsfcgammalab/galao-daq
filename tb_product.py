# =========================================================
# =========================================================
# vata4xx.py  =============================================
# IDEAS, 2018 =============================================
# =========================================================
# =========================================================

# VATA4xx python module with help functions for scripting.

# Constants, variables and functions defined here are used
# by other scripts.

# NOTE: These scripts are example scripts. They are meant 
# to show one way of operating the VATA4XX-TG with Galao.
# We encourage the user to modify these scripts according 
# to their own needs. This script was initially written for
# the VATA460, and will require modification to work with
# other ASICs in the same family.

import sys
sys.path.append('scripts/modules/')

import time
import csv
import tbScript
import tbMath
from __main__ import tb

# =========================================================
# Constants and variables =================================
# =========================================================

WRITE_DEFALT_ASIC_CONFIG        = 0
ENABLE_ALL_CH_READOUT           = False
ENABLE_INTERNAL_CK              = False
ENABLE_CAL_PULSE                = 0

ASIC_NEGATIVE_TRIGGER_POLARITY  = False
ASIC_IRAMP_DAC                  = 5
ASIC_IRAMP_FB                   = 0
ASIC_IRAMP_F2                   = 0
ASIC_IRAMP_F3                   = 0
ASIC_BIAS_DAC_IFSF_POS          = 1
ASIC_BIAS_DAC_IFSF_NEG          = 1
ASIC_BIAS_DAC_VRC_POS           = 5
ASIC_BIAS_DAC_VRC_NEG           = 0

ASIC_460_BIAS_DAC_LOFFSET_POS     = 6
ASIC_460_BIAS_DAC_LOFFSET_NEG     = 0
ASIC_460_BIAS_DAC_IFP 		= 10

ASIC_DIGITAL_THRESHOLD          = 0
ASIC_TRIGGER_THRESHOLD          = 1

CAL_PULSE_TEST_CHANNEL          = 0
CAL_PULSE_LENGTH                = 1000
CAL_PULSE_INTERVAL              = 20000
CAL_PULSE_TRIG_DELAY            = 0
CAL_PULSE_SOURCE                = 1  # 0 = External, 1 = Internal

SYS_DAC_SETTING                 = 0
INTERNAL_DAC_SETTING            = 0  
MAX_CAL_DAC                     = 4095
MAX_INTERNAL_CAL_DAC            = 127

VATA_460_NUMBER_OF_CHANNELS     = 32
VATA_450_NUMBER_OF_CHANNELS     = 64

CFG_HOLD_DLY_VATA460_POS        = 180
CFG_HOLD_DLY_VATA460_NEG        = 220
CFG_HOLD_DLY_VATA450_POS        = 300
CFG_HOLD_DLY_VATA450_NEG        = 300

g_conf_delay = 0.1

# =========================================================
# Register addresses ======================================
# =========================================================

addr_soc_temp_mon = 0x0900
addr_system_temp_mon = 0x0A04
addr_asic_avdd_mon = 0x0A00
addr_asic_avss_mon = 0x0A01

addr_sys_asic_type = 0x0020

addr_cfg_cal_en         = 0x0C00
addr_cfg_cal_polarity   = 0x0C01
addr_cfg_cal_num_pulses = 0x0C02
addr_cfg_cal_puls_length = 0x0C03
addr_cfg_cal_puls_interval = 0x0C04
addr_cfg_cal_puls_trig_delay = 0x0C05
addr_cfg_cal_puls_output_mask = 0x0C06

addr_cfg_Mbias_1_reg = 0x0E08
addr_cfg_CalDac = 0x0E0A

addr_cfg_readout_en = 0xF009
addr_cfg_Hold_dly = 0xF00A
addr_cfg_forced_en = 0xF00B

addr_cfg_InterfaceMode = 0xF00D
addr_cfg_DebugReg = 0xF00E
addr_cfg_conv_ck_period = 0xF011

# =========================================================
# ASIC addresses ==========================================
# =========================================================

bitaddr_base_Cal_DAC      = 1        
bitaddr_base_All2         = 10
bitaddr_base_Iramp_f3     = 14
bitaddr_base_Iramp_fb     = 15
bitaddr_base_Iramp_f2     = 16
bitaddr_base_RO_All       = 18
bitaddr_base_Ck_en        = 19
bitaddr_base_Cal_gen_on   = 21
bitaddr_base_Slew_on_b    = 22
bitaddr_base_Nside        = 23
bitaddr_base_Test_on      = 25
bitaddr_base_NegQ         = 27
bitaddr_base_Del_reg_0    = 41

# VATA460 -------------------------------------------------
bitaddr_base_460_Dis_chan           = 233
bitaddr_base_460_Dis_chan_0         = 234
bitaddr_base_460_DTHR               = 266
bitaddr_base_460_channel_disable_ch_0_15    = 276
bitaddr_base_460_channel_disable_ch_16_31   = 292
bitaddr_base_460_channel_trim_DAC_0 = 308
bitaddr_base_460_Test_enable_ch_0_15  = 436
bitaddr_base_460_Test_enable_ch_16_31 = 452

bitaddr_base_460_Shabi_lg           = 468
bitaddr_base_460_TriggerThreshold   = 471
bitaddr_base_460_Bias_DAC_ifp       = 476
bitaddr_base_460_Bias_DAC_Iramp     = 480
bitaddr_base_460_Bias_DAC_ck_bi     = 484
bitaddr_base_460_Bias_DAC_twbi      = 488
bitaddr_base_460_Bias_DAC_sha_bias  = 491
bitaddr_base_460_Bias_DAC_ifss      = 494
bitaddr_base_460_Bias_DAC_ifsf      = 497
bitaddr_base_460_Bias_DAC_vrc       = 500
bitaddr_base_460_Bias_DAC_sbi       = 503
bitaddr_base_460_Bias_DAC_pre_bias  = 506
bitaddr_base_460_Bias_DAC_ibuf      = 509
bitaddr_base_460_Bias_DAC_obi       = 512
bitaddr_base_460_Bias_DAC_loffset   = 515
bitaddr_base_460_Bias_DAC_disc3_bi  = 518

# VATA450 -------------------------------------------------
bitaddr_base_450_Dis_chan           = 425
bitaddr_base_450_Dis_chan_0         = 426
bitaddr_base_450_DTHR               = 490
bitaddr_base_450_channel_disable_ch_0_15 	= 500
bitaddr_base_450_channel_disable_ch_16_31   = 516
bitaddr_base_450_channel_disable_ch_32_47 	= 532
bitaddr_base_450_channel_disable_ch_48_63   = 548

bitaddr_base_450_channel_trim_DAC_0 = 564


bitaddr_base_450_Test_enable_ch_0_15  = 820
bitaddr_base_450_Test_enable_ch_16_31 = 836
bitaddr_base_450_Test_enable_ch_32_47 = 852
bitaddr_base_450_Test_enable_ch_48_63 = 868


bitaddr_base_450_Shabi_lg           = 884
bitaddr_base_450_TriggerThreshold   = 887
bitaddr_base_450_Bias_DAC_ifp       = 892
bitaddr_base_450_Bias_DAC_Iramp     = 896
bitaddr_base_450_Bias_DAC_ck_bi     = 900
bitaddr_base_450_Bias_DAC_twbi      = 904
bitaddr_base_450_Bias_DAC_sha_bias  = 907
bitaddr_base_450_Bias_DAC_ifss      = 910
bitaddr_base_450_Bias_DAC_ifsf      = 913
bitaddr_base_450_Bias_DAC_vrc       = 916
bitaddr_base_450_Bias_DAC_sbi       = 919
bitaddr_base_450_Bias_DAC_pre_bias  = 922
bitaddr_base_450_Bias_DAC_ibuf      = 925
bitaddr_base_450_Bias_DAC_obi       = 928
bitaddr_base_450_Bias_DAC_loffset   = 931
bitaddr_base_450_Bias_DAC_disc3_bi  = 934

# =========================================================
# Functions ===============================================
# =========================================================

def getAsicType():
    #print("getAsicType" )

    _CurrentASICType = ReadSysRegister(addr_sys_asic_type)
	
    return _CurrentASICType

def getMaxNumberOfChannels():
    print("getMaxNumberOfChannels" )
	
	
    if (getAsicType() == 450):
        return VATA_450_NUMBER_OF_CHANNELS
    elif (getAsicType() == 460):
        return VATA_460_NUMBER_OF_CHANNELS
    else:	
        return 0

def getDefaultHoldDelay():
    print("getMaxNumberOfChannels" )
	
		
    if (getAsicType() == 450):
        if (ASIC_NEGATIVE_TRIGGER_POLARITY):
            return CFG_HOLD_DLY_VATA450_NEG
        else:
            return CFG_HOLD_DLY_VATA450_POS
            
    elif (getAsicType() == 460):
        if (ASIC_NEGATIVE_TRIGGER_POLARITY):    
            return CFG_HOLD_DLY_VATA460_NEG
        else:
            return CFG_HOLD_DLY_VATA460_POS
            
    else:	
        return 0

def get_bitaddr_base_Bias_DAC_Iramp():
			
    if (getAsicType() == 450):
        return bitaddr_base_450_Bias_DAC_Iramp
    else:	
        return bitaddr_base_460_Bias_DAC_Iramp

# Configuration functions ---------------------------------

def ConfigureSystem():
    print("Configure System Settings" )
		
    #getAsicType()

    if (CAL_PULSE_SOURCE == 0):    
        configCalDAC(SYS_DAC_SETTING)

    if (ENABLE_CAL_PULSE == 1):
        confSysCalPulse(True)
    else:
        confSysCalPulse(False)

def ConfigureAsic(asic_id):
    print("Configure ASIC " + str(asic_id) )

    #Mbias_1
    tb.writeSysReg(addr_cfg_Mbias_1_reg, 190)    

    #Hold dly
    tb.writeSysReg(addr_cfg_Hold_dly, getDefaultHoldDelay())   
	 
    #conv_ck_period
    tb.writeSysReg(addr_cfg_conv_ck_period, 10)    
	
    #Update Debug reg to enable load    
    tb.writeSysReg(addr_cfg_DebugReg, ReadSysRegister(addr_cfg_DebugReg) or 4)    
    
    if (WRITE_DEFALT_ASIC_CONFIG > 0):
        print("Write Default Asic Config")
        confDefaultConfig(asic_id, False)
        
    #print("Enable All Channels: {0:s}".format(str(ENABLE_ALL_CH_READOUT)))
    confSingleChannel(asic_id, ENABLE_ALL_CH_READOUT, False)

    if (ENABLE_CAL_PULSE == 1):
        print("--Cal Pulses - Enabled")
        confCalPulse(asic_id, CAL_PULSE_TEST_CHANNEL, True, False)
    else:    
        print("--Cal Pulses - Disabled!")
        confCalPulse(asic_id, 0, False, False)
    
    # Configure internal Cal DAC
    if (CAL_PULSE_SOURCE == 1):
        configCalDAC(INTERNAL_DAC_SETTING)

    # Enable/Disable internal clk
    confCk_en(asic_id, ENABLE_INTERNAL_CK, False)    

    # Configure Trigger Polarity
    confTriggerPolarity(asic_id, ASIC_NEGATIVE_TRIGGER_POLARITY, False) 

    confTriggerTreshold(asic_id, ASIC_TRIGGER_THRESHOLD, False)

    # Configure Ramp Speed
    confBiasDacIramp(asic_id, ASIC_IRAMP_DAC, False)
    confRampSpeed(asic_id, ASIC_IRAMP_FB, ASIC_IRAMP_F2, ASIC_IRAMP_F3, False)

    print("..Set Digital Threshold (DTHR)")
    confDigTriggerTreshold(asic_id, ASIC_DIGITAL_THRESHOLD, True)

def confWriteConfig( asic_id ):
    print("Write Config!")
    tb.writeSysReg(addr_cfg_readout_en, 0)

    if ( ReadSysRegister(addr_cfg_InterfaceMode) > 0):
        tb.writeSysReg(addr_cfg_InterfaceMode, 1)
    
    
    time.sleep(g_conf_delay)
    tb.writeReadAsicConfig(asic_id)
    time.sleep(g_conf_delay)

def confTriggerTreshold(asic_id, TriggerTreshold, WriteConfig):

    # Set Trigger Threshold
    print("Set Trigger Threshold()")

    if (getAsicType() == 450):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_TriggerThreshold, TriggerTreshold,asic_id)
    else:
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_TriggerThreshold, TriggerTreshold,asic_id)
	
 
    print("Write Config!")
    tb.writeSysReg(addr_cfg_readout_en, 0)

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confDigTriggerTreshold(asic_id, TriggerTreshold, WriteConfig):

    # Set Trigger Threshold
    print("Set Digital Trigger Threshold({0:d})".format(TriggerTreshold))
    if (getAsicType() == 450):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_DTHR, TriggerTreshold,asic_id)
    else:
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_DTHR, TriggerTreshold,asic_id)
	
    if (WriteConfig == True):
        confWriteConfig(asic_id)
		
def confCalPulse(asic_id, Channel, enable, WriteConfig):

    print("Enable Cal Pulse")
    config_channel = 0
  
    if Channel > 111 and Channel < 129:
        Channel_base = 112		
    elif Channel > 95 and Channel < 112:
        Channel_base = 96				
    elif Channel > 79 and Channel < 96:
        Channel_base = 80	
    elif Channel > 63 and Channel < 80:
        Channel_base = 64			
    elif Channel > 47 and Channel < 64:
        Channel_base = 48
    elif Channel > 31 and Channel < 48:
        Channel_base = 32		
    elif Channel > 15 and Channel < 32:
        Channel_base = 16		
    elif Channel >= 0 and Channel < 16:
        Channel_base = 0
    else:    
        config_channel = 0
        Channel_base = 0

   
    channel_bit = 1
    config_channel = channel_bit << (Channel - Channel_base) 

    # print("Channel = " + str(Channel) + " Channel Base = " + str(Channel_base) + " Channel Config = "+ str(config_channel))
	
    config_channel_0_0 = 0
    config_channel_0_1 = 0
    config_channel_1_0 = 0
    config_channel_1_1 = 0
    config_channel_2_0 = 0
    config_channel_2_1 = 0
    config_channel_3_0 = 0
    config_channel_3_1 = 0

    if (enable == False):
        config_channel = 0
        	
    if Channel > 111 and Channel < 129:
        config_channel_3_1 = config_channel		
    if Channel > 95 and Channel < 112:
        config_channel_3_0 = config_channel				
    elif Channel > 79 and Channel < 96:
        config_channel_2_1 = config_channel		
    elif Channel > 63 and Channel < 80:
        config_channel_2_0 = config_channel				
    elif Channel > 47 and Channel < 64:
        config_channel_1_1 = config_channel		
    elif Channel > 31 and Channel < 48:
        config_channel_1_0 = config_channel				
    elif Channel > 15 and Channel < 32:
        config_channel_0_1 = config_channel
    elif Channel >= 0 and Channel < 16:
        config_channel_0_0 = config_channel		
    else:    
        config_channel = 0

    
    if (getAsicType() == 450):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_0_15, config_channel_0_0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_16_31, config_channel_0_1, asic_id)     
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_32_47, config_channel_1_0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_48_63, config_channel_1_1, asic_id) 

    else:
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_0_15, config_channel_0_0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_16_31, config_channel_0_1, asic_id)     
    
#    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_enable_ch_64_79, config_channel_2_0, asic_id) 
#    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_enable_ch_80_95, config_channel_2_1, asic_id) 
#    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_enable_ch_96_111, config_channel_3_0, asic_id) 
#    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_enable_ch_112_127, config_channel_3_1, asic_id) 

    if (CAL_PULSE_SOURCE == 0):
        #Disable the internal cal generator
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Cal_gen_on, 0,asic_id)	
    else:
        #Enable the internal cal generator
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Cal_gen_on, 1,asic_id)	
		
    if (getAsicType() == 460):
        if (ASIC_NEGATIVE_TRIGGER_POLARITY == True):
            # Disable slew rate limited fast shaper
            tb.setAsicConfigBitFieldByAddr(bitaddr_base_Slew_on_b, 1, asic_id) 
        else:
            # Enable slew rate limited fast shaper
            tb.setAsicConfigBitFieldByAddr(bitaddr_base_Slew_on_b, 0, asic_id) 	
	
    if (enable == True):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_on, 1,asic_id)
    else:   
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Test_on, 0,asic_id) 

    if (WriteConfig == True):
        confWriteConfig(asic_id)
        
def confDefaultConfig_VATA450(asic_id, ModeIO):

    print("Default Config VATA450")

    tb.setAsicConfigBitFieldByAddr(1, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(8, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(10, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(11, 0, asic_id)
	
    tb.setAsicConfigBitFieldByAddr(15, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(16, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(17, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(18, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(19, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(20, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(21, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(22, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(23, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(24, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(25, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(26, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(27, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(28, 0, asic_id)
    
    if (ModeIO == True):
        tb.setAsicConfigBitFieldByAddr(29, 0, asic_id)    
        tb.setAsicConfigBitFieldByAddr(30, 0, asic_id)
    else:    
        tb.setAsicConfigBitFieldByAddr(29, 1, asic_id)        
        tb.setAsicConfigBitFieldByAddr(30, 1, asic_id)
        
    tb.setAsicConfigBitFieldByAddr(31, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(32, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(33, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(34, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(35, 0, asic_id)

    #Config Delay registers
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_Del_reg_0 + i*6 
        print("Default Del_reg config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Config Disable CM Channel
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Dis_chan, 0, asic_id)
	
    #Config Dis_chan registers
    bitaddr_base_Dis_chan_0 = bitaddr_base_450_Dis_chan_0
	
	
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_Dis_chan_0 + i*1 
        print("Default Dis_chan config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Dig Threshold    
    confDigTriggerTreshold(asic_id, 0, False)
    
    #channel_disable
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_channel_disable_ch_0_15, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_channel_disable_ch_16_31, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_channel_disable_ch_32_47, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_channel_disable_ch_48_63, 0, asic_id)


	
    #Config Channel_trim_DAC
    bitaddr_base_channel_trim_DAC_0 = bitaddr_base_450_channel_trim_DAC_0
		
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_channel_trim_DAC_0 + i*4 
        print("Default Channel_trim_DAC config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Test Enable
    if (getAsicType() == 450):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_0_15, 0, asic_id)    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_16_31, 0, asic_id)  
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_32_47, 0, asic_id)    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_48_63, 0, asic_id)  

    else:
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_0_15, 0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_16_31, 0, asic_id)     
		
	
    #Shabi_lg
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Shabi_lg, 0, asic_id)
		
    #Pos_il_1
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Shabi_lg + 1, 0, asic_id)
		
    #Pos_il_2
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Shabi_lg + 2, 0, asic_id)
	
    #Trigger Threshold
    confTriggerTreshold(asic_id, 0, False)

    #Bias_DAC_ifp
    #Pos_il_2
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ifp, 0, asic_id)
		
    #Bias_DAC_Iramp
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_Iramp, 0, asic_id)

    #Bias_DAC_ck_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ck_bi, 0, asic_id)
		
    #Bias_DAC_twbi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_twbi, 0, asic_id)
	
    #Bias_DAC_sha_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_sha_bias, 0, asic_id)
	
    #Bias_DAC_ifss
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ifss, 0, asic_id)
	
    if (ASIC_NEGATIVE_TRIGGER_POLARITY):    
        #Bias_DAC_ifsf		
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ifsf, ASIC_BIAS_DAC_IFSF_NEG, asic_id)
        #Bias_DAC_vrc    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_vrc, ASIC_BIAS_DAC_VRC_NEG, asic_id)        
    else:   
        #Bias_DAC_ifsf		    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ifsf, ASIC_BIAS_DAC_IFSF_POS, asic_id) 
        #Bias_DAC_vrc    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_vrc, ASIC_BIAS_DAC_VRC_POS, asic_id)        
		

    #Bias_DAC_sbi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_sbi, 0, asic_id)
	
    #Bias_DAC_pre_bias
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_pre_bias, 0, asic_id)
	
    #Bias_DAC_ibuf
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_ibuf, 0, asic_id)
	
    #Bias_DAC_obi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_obi, 0, asic_id)

    #Bias_DAC_loffset
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_loffset, 0, asic_id)

    #Bias_DAC_dic3_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Bias_DAC_disc3_bi, 0, asic_id)

def confDefaultConfig_VATA460(asic_id, ModeIO):

    print("Default Config VATA460")

    tb.setAsicConfigBitFieldByAddr(1, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(8, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(10, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(11, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(14, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(15, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(16, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(17, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(18, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(19, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(20, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(21, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(22, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(23, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(24, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(25, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(26, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(27, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(28, 0, asic_id)
    
    if (ModeIO == True):
        tb.setAsicConfigBitFieldByAddr(29, 0, asic_id)    
        tb.setAsicConfigBitFieldByAddr(30, 0, asic_id)
    else:    
        tb.setAsicConfigBitFieldByAddr(29, 1, asic_id)        
        tb.setAsicConfigBitFieldByAddr(30, 1, asic_id)
        
    tb.setAsicConfigBitFieldByAddr(31, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(32, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(33, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(34, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(35, 0, asic_id)

    #Config Delay registers
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_Del_reg_0 + i*6 
        print("Default Del_reg config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Config Disable CM Channel
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Dis_chan, 0, asic_id)

    #Config Dis_chan registers
    bitaddr_base_Dis_chan_0 = bitaddr_base_460_Dis_chan_0
	
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_Dis_chan_0 + i*1 
        print("Default Dis_chan config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Dig Threshold    
    confDigTriggerTreshold(asic_id, 0, False)

    
    #channel_disable
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_channel_disable_ch_0_15, 0, asic_id)
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_channel_disable_ch_16_31, 0, asic_id)

    #Config Channel_trim_DAC
    bitaddr_base_channel_trim_DAC_0 = bitaddr_base_460_channel_trim_DAC_0
		
    for i in range(0,getMaxNumberOfChannels()):
        config_addr = bitaddr_base_channel_trim_DAC_0 + i*4 
        print("Default Channel_trim_DAC config Addr = " + str(config_addr))
        tb.setAsicConfigBitFieldByAddr(config_addr, 0, asic_id)

    #Test Enable
    if (getAsicType() == 450):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_0_15, 0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_16_31, 0, asic_id)     
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_32_47, 0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_450_Test_enable_ch_48_63, 0, asic_id) 
		
    else:
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_0_15, 0, asic_id) 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Test_enable_ch_16_31, 0, asic_id)     
    
	
    #Shabi_lg
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Shabi_lg, 0, asic_id)
		
    #Pos_il_1
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Shabi_lg + 1, 0, asic_id)
		
    #Pos_il_2
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Shabi_lg + 2, 0, asic_id)
	
    #Trigger Threshold
    confTriggerTreshold(asic_id, 0, False)

    #Bias_DAC_ifp
    #Pos_il_2
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ifp, ASIC_460_BIAS_DAC_IFP, asic_id)
		
    #Bias_DAC_Iramp
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_Iramp, 0, asic_id)

    #Bias_DAC_ck_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ck_bi, 0, asic_id)
		
    #Bias_DAC_twbi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_twbi, 0, asic_id)
	

    #Bias_DAC_sha_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_sha_bias, 0, asic_id)
	
    #Bias_DAC_ifss
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ifss, 0, asic_id)
	
    if (ASIC_NEGATIVE_TRIGGER_POLARITY):    
        #Bias_DAC_ifsf		
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ifsf, ASIC_BIAS_DAC_IFSF_NEG, asic_id)
        #Bias_DAC_vrc    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_vrc, ASIC_BIAS_DAC_VRC_NEG, asic_id)        
    else:   
        #Bias_DAC_ifsf		    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ifsf, ASIC_BIAS_DAC_IFSF_POS, asic_id) 
        #Bias_DAC_vrc    
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_vrc, ASIC_BIAS_DAC_VRC_POS, asic_id)        
        
    #Bias_DAC_sbi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_sbi, 0, asic_id)

    #Bias_DAC_pre_bias
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_pre_bias, 0, asic_id)

    #Bias_DAC_ibuf
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_ibuf, 0, asic_id)

    #Bias_DAC_obi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_obi, 0, asic_id)

    if (ASIC_NEGATIVE_TRIGGER_POLARITY):    
        #Bias_DAC_loffset	
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_loffset, ASIC_460_BIAS_DAC_LOFFSET_NEG, asic_id)
    else:
        #Bias_DAC_loffset	
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_loffset, ASIC_460_BIAS_DAC_LOFFSET_POS, asic_id)
	
    #Bias_DAC_dic3_bi
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_460_Bias_DAC_disc3_bi, 0, asic_id)

    print("--- --- Done writing default ASIC configuration. --- ---\n")
    
def confDefaultConfig(asic_id, WriteConfig ):

    print("Write Default ASIC Config")

    tb.writeSysReg(addr_cfg_readout_en, 0)
    
    #Check Config mode
    if ( ReadSysRegister(addr_cfg_InterfaceMode) > 0):
        ModeIO = True
    else:
        ModeIO = False
    
    if (ModeIO == False):
        #Set Mode Interface = 0
        tb.writeSysReg(addr_cfg_InterfaceMode, 0)
    else:
        tb.writeSysReg(addr_cfg_InterfaceMode, 1)
          
    if (getAsicType() == 450):
        confDefaultConfig_VATA450(asic_id, ModeIO)
    elif (getAsicType() == 460):
        confDefaultConfig_VATA460(asic_id, ModeIO)
    else:
        print("Error ! Unsupported ASIC Type")

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confDelayRegSingleChannel(asic_id, Channel, Delay, WriteConfig):

    if (Channel < getMaxNumberOfChannels()):
        conf_ch = Channel
    else:    
        conf_ch = 0
        
    config_addr = bitaddr_base_Del_reg_0 + conf_ch*6     
    
    tb.setAsicConfigBitFieldByAddr(config_addr, Delay, asic_id)

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confSingleChannel(asic_id, EnableAllChannels, WriteConfig):

    print("Enable Single/All Channels")
    
    if (EnableAllChannels == True):
        print("--All Channel Readout")
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_All2, 1,asic_id)
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_RO_All, 1,asic_id)
    else:   
        print("--Single Channel Readout")
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_All2, 0,asic_id)
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_RO_All, 0,asic_id)

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confTriggerPolarity(asic_id, negativ_polarity, WriteConfig):

    print("Configure Trigger Polarity")
    
    if (negativ_polarity == True):
        print("--Negative Trigger Polarity")
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Nside, 1,asic_id)
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_NegQ, 1,asic_id)
    else:   
        print("--Positive Trigger Polarity")
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Nside, 0,asic_id)
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_NegQ, 0,asic_id)

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confCk_en(asic_id, enable, WriteConfig):

    print("Configure Ck_En")
    
    if (enable == True):
        print("--Enable Internal Ck")
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Ck_en, 1,asic_id)
    else:  
        print("--Disable Internal Ck") 
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Ck_en, 0,asic_id)

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confBiasDacIramp(asic_id, DAC_Value, WriteConfig):

    if (DAC_Value < 16):
        asic_value = DAC_Value
    else:
        asic_value = 15

    tb.setAsicConfigBitFieldByAddr(get_bitaddr_base_Bias_DAC_Iramp(), asic_value,asic_id)

    print("Configure Bias DAC iRamp = " + str(asic_value) )

    if (WriteConfig == True):
        confWriteConfig(asic_id)

def confRampSpeed(asic_id, iramp_fb, iramp_f2, iramp_f3, WriteConfig):

    if (iramp_fb > 0):
        asic_fb = 1
    else:
        asic_fb = 0

    if (iramp_f2 > 0):
        asic_f2 = 1
    else:
        asic_f2 = 0

    if (iramp_f3 > 0):
        asic_f3 = 1
    else:
        asic_f3 = 0
            
    print("Configure Ramp Speed, Iramp_f3 = " + str(asic_f3) + " Iramp_f2 = " + str(asic_f2) + " Iramp_fb = " + str(asic_fb) )

    if (getAsicType() == 460):
        tb.setAsicConfigBitFieldByAddr(bitaddr_base_Iramp_f3, asic_f3 ,asic_id)
	
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Iramp_f2, asic_f2 ,asic_id)        
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Iramp_fb, asic_fb ,asic_id)        

    if (WriteConfig == True):
        confWriteConfig(asic_id)

# System functions ----------------------------------------
def EnableForcedReadout(Enable):
    print("Enable/disable forced Readout")
	
    tb.writeSysReg(addr_cfg_forced_en, Enable)


def ReadSysRegister(register_address):
    dw_rd = 0
    tb.readSysReg(register_address)

    time.sleep(g_conf_delay)
    
    dw_rd =  tb.getSysRegValue(register_address)
    
    return dw_rd

def confSysCalPulse(enable):
    
    if (enable == True):
        print("Configure Cal pulse - Enable")
    else:
        print("Configure Cal pulse - Disable")

    if (enable == True):

        if (ASIC_NEGATIVE_TRIGGER_POLARITY) or (CAL_PULSE_SOURCE):    

            tb.writeSysReg(addr_cfg_cal_polarity, 1)                        
        else:
            tb.writeSysReg(addr_cfg_cal_polarity, 0) 
            
        tb.writeSysReg(addr_cfg_cal_puls_length, CAL_PULSE_LENGTH)
        tb.writeSysReg(addr_cfg_cal_puls_interval, CAL_PULSE_INTERVAL)
        tb.writeSysReg(addr_cfg_cal_puls_trig_delay, CAL_PULSE_TRIG_DELAY)

        time.sleep(g_conf_delay)

        if (CAL_PULSE_SOURCE == 0):
            #External calibration generator    
            tb.writeSysReg(addr_cfg_cal_puls_output_mask, 1)            
        else:
            #Internal calibration generator
            tb.writeSysReg(addr_cfg_cal_puls_output_mask, 2)
        tb.writeSysReg(addr_cfg_cal_en, 1)
    else:
        tb.writeSysReg(addr_cfg_cal_puls_output_mask, 0)
                
def confCalDAC( dac_setting ):

    if (dac_setting < MAX_CAL_DAC):
        dac_value = dac_setting
    else:    
        dac_value = MAX_CAL_DAC

    print("Configure Cal DAC = " + str(dac_value))    
    tb.writeSysReg(addr_cfg_CalDac, dac_value)    
    return dac_value	

def configInternalCalDAC( dac_setting ):

    if (dac_setting < MAX_INTERNAL_CAL_DAC):
        dac_value = dac_setting
    else:    
        dac_value = MAX_INTERNAL_CAL_DAC
	    
    print("Configure Internal Cal DAC = " + str(dac_value))    
    tb.setAsicConfigBitFieldByAddr(bitaddr_base_Cal_DAC, dac_value, 0) 
    confWriteConfig(0)
    time.sleep(g_conf_delay)
	
    return dac_value
		
def configCalDAC( dac_setting ):

    if (CAL_PULSE_SOURCE == 0): # External Cal DAC
        return confCalDAC(dac_setting)
    else:    
        return configInternalCalDAC(dac_setting) # Internal Cal DAC
