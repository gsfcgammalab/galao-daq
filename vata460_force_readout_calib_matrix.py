#SG: Script for calibration pulse injection. 
# Uses modified configuration files wrt other programs!

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')
import tbScript
import tb_product as vata4xx #legacy
import vata4xxHelp as vh
import os
import datetime
reload(vh)
reload(vata4xx)

import time
import numpy as np

import yaml

from shutil import copyfile


# ---------- LOAD CONFIGURATION FILE ------------------ #
config_filename = ".\scripts/Galao-VATA4xx/config/forced_trigger_readout_calib_matrix.yaml"
configuration = yaml.load(open(config_filename, 'r'))

# -------- START OF SCRIPT ---------
print("\n\n-")
print("--------------------------------------")
print("VATA460 Calibration Matrix Readout")
print("--------------------------------------")
print("\n\n-")

'''
print("Setting ASIC configuration...")
### Configure ASIC
cfg = configuration['ASIC_settings'] 
print(cfg.keys())
vata4xx.WRITE_DEFALT_ASIC_CONFIG = 1 #Force write default ASIC configuration. 
vata4xx.ENABLE_ALL_CH_READOUT = cfg['enable_all_ch_readout']
vata4xx.ASIC_NEGATIVE_TRIGGER_POLARITY = True if cfg['polarity']  < 0  else False
vata4xx.ASIC_DIGITAL_THRESHOLD = cfg['digital_threshold']
vata4xx.ASIC_TRIGGER_THRESHOLD = cfg['trigger_threshold']
vata4xx.CAL_PULSE_TEST_CHANNEL = 15 #may get overwritten depending on run type

tb.setAsicConfigBitFieldByAddr(17, cfg['disable_CM_subtraction'], 0)

### Configure ASIC dynamic range
cfg = cfg['iramp_settings']
vata4xx.ASIC_IRAMP_DAC = cfg['DAC']
vata4xx.ASIC_IRAMP_FB  = cfg['FB']
vata4xx.ASIC_IRAMP_F2  = cfg['F2']
vata4xx.ASIC_IRAMP_F3  = cfg['F3']


print("-----------------------------------")
print("Setting QI configuration...")
### Configure QI; applies to ASIC if internal calibrator is being used.
cfg = configuration['QI_settings']
vata4xx.CAL_PULSE_SOURCE = cfg['cal_source'] # 0 = external, 1 = internal

if vata4xx.CAL_PULSE_SOURCE == 0:
    print("*** Using external calibrator.")
    vata4xx.SYS_DAC_SETTING        = cfg['SYS_DAC_SETTING']
else:
    print("*** Using internal calibrator.")
    vata4xx.INTERNAL_DAC_SETTING   = cfg['INTERNAL_DAC_SETTING']

vata4xx.CAL_PULSE_TEST_CHANNEL = cfg['target_channel']
vata4xx.CAL_PULSE_INTERVAL     = cfg['pulse_interval'] -1 
vata4xx.CAL_PULSE_LENGTH       = cfg['pulse_length'] -1
#vata4xx.CAL_PULSE_TRIG_DELAY   = cfg['trigger_delay']
vata4xx.ENABLE_CAL_PULSE       = cfg['enable_cal_pulse'] 
tb.writeSysReg(vata4xx.addr_cfg_forced_en, cfg['forced_trigger_readout'])


vata4xx.ConfigureAsic(0) #writes registers
vh.configure_asic() #commits send
vata4xx.ConfigureSystem()
vh.configure_asic() #commits send
'''

vh.set_start_config()
vh.load_and_set_config(configuration)


#Setup datataking.
date = datetime.date.today()
date_str = "d{0:d}{1:02d}{2:02d}".format(date.year, date.month, date.day)

dataset_name = configuration['general']['filename']
if dataset_name == 0:
    dataset_name = None

basename = "C:/work/detector/data/{0:s}/{1:s}".format(date_str, dataset_name)
target_cfg = basename + "/config.txt"
write_file = "{0:s}/data.csv".format(basename)

vh.write_cfg_file_copy(dataset_name, basename, target_cfg)
vh.clear_existing_target_file(write_file):
vh.start_new_log(write_file)
time.sleep(vata4xx.g_conf_delay)

run_length = configuration['general']['duration']
print("Run length: {0:.2f}s".format(run_length))


#GO!
vh.enable_readout(reset=False)    
time.sleep(run_length)
vh.disable_readout()

vh.start_new_log()
tb.enableDataLogging(False)
vh.enable_readout(False)

# -------- END OF SCRIPT ----------
print("Done")

