# =========================================================
# vata4xx-02-enable_cal_pulse.py ==========================
# IDEAS, 2018                    ==========================
# =========================================================
# =========================================================

# Starts sending calibration test pulses to a chosen
# channel. Can be used after one of the startup scripts
# have been executed.

# NOTE: These scripts are example scripts. They are meant 
# to show one way of operating the VATA4XX-TG with Galao.
# We encourage the user to modify these scripts according 
# to their own needs. This script was initially written for
# the VATA460, and will require modification to work with
# other ASICs in the same family.

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')

import tbScript
import tb_product

# =========================================================
# User settings ===========================================
# =========================================================

tb_product.CAL_PULSE_TEST_CHANNEL  = 7
tb_product.CAL_PULSE_SOURCE        = 0    # 0 = External, 1 = Internal
tb_product.CAL_PULSE_INTERVAL      = 100000 * 4
tb_product.CAL_PULSE_LENGTH        = 1000
tb_product.CAL_PULSE_TRIG_DELAY    = 30

tb_product.ASIC_DIGITAL_THRESHOLD  = 100
tb_product.ASIC_TRIGGER_THRESHOLD  = 2

tb_product.SYS_DAC_SETTING         = 1200
tb_product.INTERNAL_DAC_SETTING    = 50

# =========================================================
# Script start ============================================
# =========================================================

print("VATA4xx enable cal pulse script started.")

tb_product.WRITE_DEFALT_ASIC_CONFIG = 0
tb_product.ENABLE_CAL_PULSE = 1

# Turn on Data Processing / GUI functions (for baseline readout)
tb.enableGuiUpdates(True)
tb.enablePlotUpdates(True)
tb.enableDataProcessing(True)
tb.enableHistograms(True)

# Disable readout -----------------------------------------

print("Disabling readout.")
tb.writeSysReg(tb_product.addr_cfg_readout_en, 0)

# Configure ASIC ------------------------------------------

print("Configuring ASIC.")
tb_product.ConfigureAsic(0)

# Configure system ----------------------------------------

print("Configuring system.")
tb_product.ConfigureSystem()
tb_product.confTriggerTreshold(0,tb_product.ASIC_TRIGGER_THRESHOLD, True)

# Enable readout ------------------------------------------

print("Enabling readout.")
tb.writeSysReg(tb_product.addr_cfg_readout_en, 1)

print("VATA4xx enable cal pulse script finished.")
print("Calibration test charges are now sent to the selected channel.")