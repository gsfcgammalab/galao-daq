# =========================================================
# =========================================================
# vata4xx-03-acquisition.py ===============================
# IDEAS, 2018               ===============================
# =========================================================
# =========================================================

# Records a specified number of events to a separate log file.
# Can be used while readouts are performed.

# NOTE: These scripts are example scripts. They are meant 
# to show one way of operating the VATA4XX-TG with Galao.
# We encourage the user to modify these scripts according 
# to their own needs. This script was initially written for
# the VATA460, and will require modification to work with
# other ASICs in the same family.

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')

import tbScript
import tb_product
import datetime

# =========================================================
# User settings ===========================================
# =========================================================

num_readouts    = 1000
wait_time       = 60

datalog_filename = datetime.datetime.now().strftime('.\Logs\%Y-%m-%d__%H_%M_%S__vata4xx_acq_log.csv')

# =========================================================
# Script start ============================================
# =========================================================

print("VATA4xx acquisition script started.")

# Reset data ----------------------------------------------

print("Resetting readout data.")
tb.writeSysReg(tb_product.addr_cfg_readout_en, 0)
tbScript.confDelay()
tb.resetReadoutData()
tbScript.confDelay()

# Setup logging -------------------------------------------

tb.enableDataLogging(False)
tb.newDataLogFile(datalog_filename)
tb.enableDataLogging(True)

# Start acqusition ----------------------------------------

if wait_time < 5:
    wait_time = 5

print("Starting a finite readout sequence (" + str(wait_time) + " seconds).")

tb.writeSysReg(tb_product.addr_cfg_readout_en, 1)
tbScript.waitForData(num_readouts, wait_time)

# Stop acquisition ----------------------------------------

print("Stopping readout and logging.")

tb.writeSysReg(tb_product.addr_cfg_readout_en, 0)
tb.enableDataLogging(False)

print("Received " + str(tb.getEventNum()) + " readout.")

print("Closing log file.")
datalog_new_filename = datetime.datetime.now().strftime('.\Logs\%Y-%m-%d__%H_%M_%S__VATA4xx__raw_data_log.csv')

tb.newDataLogFile(datalog_new_filename)

print("VATA4xx acquisition script finished.")