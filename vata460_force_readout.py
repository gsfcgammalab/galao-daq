#SG: Script for calibration pulse injection. 
# Uses modified configuration files wrt other programs!

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')
import tbScript
import tb_product as vata4xx #legacy
import vata4xxHelp as vh
import os
import datetime
reload(vh)
reload(vata4xx)

import time
import numpy as np

import yaml

from shutil import copyfile


# ---------- LOAD CONFIGURATION FILE ------------------ #
config_filename = ".\scripts/Galao-VATA4xx/config/forced_trigger_readout.yaml"
configuration = yaml.load(open(config_filename, 'r'))

# -------- START OF SCRIPT ---------
print("\n\n-")
print("--------------------------------------")
print("VATA460 Forced Trigger Readout")
print("--------------------------------------")
print("\n\n-")

vh.set_start_config()
vh.load_and_set_config(configuration)

#Setup datataking.
date = datetime.date.today()
date_str = "d{0:d}{1:02d}{2:02d}".format(date.year, date.month, date.day)

dataset_name = configuration['general']['filename']
if dataset_name == 0:
    dataset_name = None

basename = "C:/work/data/{0:s}/{1:s}".formabt(date_str, dataset_name)
target_cfg = basename + "/fr_config.txt"
write_file = "{0:s}/data.csv".format(basename)

vh.write_cfg_file_copy(dataset_name, basename, target_cfg)
vh.clear_existing_target_file(write_file):
vh.start_new_log(write_file)
time.sleep(vata4xx.g_conf_delay)

run_length = configuration['general']['duration']
print("Run length: {0:.2f}s".format(run_length))


#GO!
vh.enable_readout(reset=False)    
time.sleep(run_length)
vh.disable_readout()

vh.start_new_log()
tb.enableDataLogging(False)
vh.enable_readout(False)

# -------- END OF SCRIPT ----------
print("Done")

