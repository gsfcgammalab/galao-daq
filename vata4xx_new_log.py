# =========================================================
# =========================================================
# vata4xx-04-new_log.py ===================================
# IDEAS, 2018           ===================================
# =========================================================
# =========================================================

# Creates a new raw data log file.

# NOTE: These scripts are example scripts. They are meant 
# to show one way of operating the VATA4XX-TG with Galao.
# We encourage the user to modify these scripts according 
# to their own needs. This script was initially written for
# the VATA460, and will require modification to work with
# other ASICs in the same family.

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')

import tbScript
import tb_product
import datetime

# =========================================================
# Script start ============================================
# =========================================================

datalog_new_filename = datetime.datetime.now().strftime('.\Logs\%Y-%m-%d__%H_%M_%S__VATA4xx__raw_data_log.csv')

tb.newDataLogFile(datalog_new_filename)