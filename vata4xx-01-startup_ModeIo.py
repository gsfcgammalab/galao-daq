# =========================================================
# =========================================================
# vata4xx-01-startup_ModeIo.py ============================
# IDEAS, 2018                  ============================
# =========================================================
# =========================================================

# Sets the system up for performing readouts with positive
# charges using the Mode IO interface. Only the triggered
# channel is read out at a time. 

# Script assumes that there is no jumper inserted on JH1 on
# the test board.

# NOTE: These scripts are example scripts. They are meant 
# to show one way of operating the VATA4XX-TG with Galao.
# We encourage the user to modify these scripts according 
# to their own needs. This script was initially written for
# the VATA460, and will require modification to work with
# other ASICs in the same family.

import sys
sys.path.append('scripts/modules')
sys.path.append('scripts/Galao-VATA4xx/')

import tbScript
import tb_product

# =========================================================
# User settings ===========================================
# =========================================================

tb_product.ASIC_DIGITAL_THRESHOLD = 10
tb_product.ASIC_TRIGGER_THRESHOLD = 10

HIST_NUMBER_OF_BINS = 1024
HIST_MIN_VALUE      = 0

# =========================================================
# System configuration ====================================
# =========================================================

tb_product.WRITE_DEFALT_ASIC_CONFIG        = 1
tb_product.ENABLE_ALL_CH_READOUT           = False
tb_product.ASIC_NEGATIVE_TRIGGER_POLARITY  = False
tb_product.CAL_PULSE_TEST_CHANNEL          = 0
tb_product.SYS_DAC_SETTING                 = 0

if (tb_product.ASIC_NEGATIVE_TRIGGER_POLARITY == True):
    ASIC_IRAMP_DAC = 4
    ASIC_IRAMP_FB = 0
    ASIC_IRAMP_F2 = 0
    ASIC_IRAMP_F3 = 0
else:
    ASIC_IRAMP_DAC = 5
    ASIC_IRAMP_FB = 0
    ASIC_IRAMP_F2 = 0
    ASIC_IRAMP_F3 = 0

INTERFACE_MODE = 1

# =========================================================
# Script start ============================================
# =========================================================

print("VATA4xx startup Mode IO script (pos) started.")

# Data processing and GUI functions -----------------------

tb.enableGuiUpdates(True)
tb.enablePlotUpdates(True)
tb.enableDataProcessing(True)
tb.enableHistograms(True)

# Setup histogram -----------------------------------------

tb.setHistogramMaxValue(HIST_NUMBER_OF_BINS - 1)
tb.setHistogramNumBins(HIST_NUMBER_OF_BINS)
tb.setHistogramMinValue(HIST_MIN_VALUE)

# Disable readout -----------------------------------------

print("Disabling readout.")
tb.writeSysReg(tb_product.addr_cfg_readout_en, 0)

# Set interface mode --------------------------------------

print("Setting interface mode.")
tb.writeSysReg(tb_product.addr_cfg_InterfaceMode, INTERFACE_MODE)

tb_product.ConfigureAsic(0)
tb_product.confBiasDacIramp(0, ASIC_IRAMP_DAC, True)
tb_product.ConfigureSystem()
tb_product.confTriggerTreshold(0,tb_product.ASIC_TRIGGER_THRESHOLD, True)

tb_product.confRampSpeed(0, ASIC_IRAMP_FB, ASIC_IRAMP_F2, ASIC_IRAMP_F3, False)
tb_product.confBiasDacIramp(0, ASIC_IRAMP_DAC, True)

# Enable Readout ------------------------------------------

print("Enabling readout.")
tb.writeSysReg(tb_product.addr_cfg_readout_en, 1)

print("VATA4xx startup Mode IO script (pos) finished.")
print("Readouts in Mode IO with positive charges are now enabled.")